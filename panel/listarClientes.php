<!DOCTYPE html>
<html lang="en">
<head>
    <?php require_once("views/utils/head.php"); ?>
    <title>Sinapse | Clientes Cadastrados</title>
</head>
<body>
<?php require_once("views/cadastro/containerNavs.php"); ?>
<?php require_once("views/cadastro/containerList.php"); ?>
</body>
</html>